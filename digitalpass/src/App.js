import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './js/header';
import Home from './js/home';
import About from './js/about';
import Contact from './js/contact';
import FeedbackForm from './js/feedback';
import SignInAForm from "./js/login";
import RegistrationForm from './js/signup';
import Index from './js/indexpage';
import './App.css';
import Certificate from './js/Certificates';
import Dashboard from './js/Dashboard';
import Feedback from './js/Feedbacks';
import Sidebar from "./js/Sidebar";
import Web3 from 'web3';
import { CERTIFICATE_LIST_ABI, CERTIFICATE_LIST_ADDRESS } from './abi/config_certificate';

class App extends Component {
  state = {
    account: '',
    balance: '',
    certificateContract: null,
    certificateNumber: 0,
    certificates: [],
  };

  componentDidMount() {
    this.loadBlockchainData();
    this.loadMetaMask();
  }

  async loadMetaMask() {
    try {
      if (!window.ethereum)
        console.log('No crypto wallet found. Please install it.');

      await window.ethereum.send('eth_requestAccounts');
      const web3 = new Web3(Web3.givenProvider || 'https://sepolia.infura.io/v3/620c9218aca74fe3991ec94f5ca0bb3a');
      const accounts = await web3.eth.getAccounts();
      const balance = await web3.eth.getBalance(accounts[0]);
      const ethValue = web3.utils.fromWei(balance, 'ether');
      this.setState({ account: accounts[0], balance: ethValue });
    } catch (error) {
      console.log(error);
    }
  }

  async loadBlockchainData() {
    try {
      const web3 = new Web3(Web3.givenProvider || 'https://sepolia.infura.io/v3/620c9218aca74fe3991ec94f5ca0bb3a');
      const certificateContract = new web3.eth.Contract(
        CERTIFICATE_LIST_ABI,
        CERTIFICATE_LIST_ADDRESS
      );
      this.setState({ certificateContract });
      const certificateNumber = await certificateContract.methods.certificateNumber().call();
      this.setState({ certificateNumber });
      console.log(certificateNumber);

      const certificates = [];
      for (let i = 0; i < certificateNumber; i++) {
        const certificate = await certificateContract.methods.certificates(i).call();
        certificates.push(certificate);
      }
      this.setState({ certificates }, () => {
        console.log('certificates: ', this.state.certificates);
      });
    } catch (error) {
      console.error(error.message);
    }
  }

  render() {
    return (
      <Router>
        <div className="app">
          <Routes>
            <Route path="/login" element={<SignInAForm />} />
            <Route path="/signup" element={<RegistrationForm />} />

            <Route path="/" element={<Header />}>
              <Route index element={<Home />} />
              <Route path="about" element={<About />} />
              <Route path="index" element={<Index certificateContract={this.state.certificateContract} />} />
              <Route path="contact" element={<Contact />} />
              <Route path="feedback" element={<FeedbackForm />} />
            </Route>
            <Route path="/" element={<Sidebar />}>
              <Route index path="/db" element={<Dashboard certificateContract={this.state.certificateContract} />} />
              <Route path="/certificate" element={<Certificate certificateContract={this.state.certificateContract} />} />
              <Route path="/adminfeedback" element={<Feedback />} />
            </Route>
          </Routes>
        </div>
      </Router>
    );
  }
}

export default App;
