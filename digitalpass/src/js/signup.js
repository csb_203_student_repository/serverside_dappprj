import React, { useState } from 'react';
import '../css/signup.css';
import axios from 'axios';
import { toast,ToastContainer } from 'react-toastify';
import { useNavigate } from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';


const RegistrationForm = () => {
  const navigate = useNavigate()
  const toastOption = {
    position: "top-center",
    autoClose: 2000,
    pauseOnHover: true,
    draggable: true,
    theme: "dark",
  }

  const [username, setUsername] = useState('');
  const [enrollmentNo, setenrollmentNo] = useState('');
  const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isChecked, setIsChecked] = useState(false);


  const handleSignup = async (e) => {
    e.preventDefault()

    // alert("skdjhkjshdkj")
    try {
      const respond = await axios({
        method: "POST",
        url: "http://localhost:4005/api/v1/users/signup",
        data: {
          name: username,
          enrollmentNumber: enrollmentNo,
          email: email,
          password: password,
          passwordConfirm: confirmPassword
        }
      })
      console.log(respond.data.data.user)
      if (respond.data.status === "success") {
        toast.success("Registeration Successfull",toastOption)
        setTimeout(() => {
          navigate("/login")
        }, 3000);
      }
    } catch (err) {
      console.log(err)
    }
  }





  function handleChange(event) {
    setIsChecked(event.target.checked);
  }



  return (
    <form className="registration-form2" >
      <ToastContainer/>
      <h2>Register Now!</h2>
      <div className="form-group2">
        <label htmlFor="username">Full name</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
          required
        />
      </div>
      <div className="form-group2">
        <label htmlFor="enrollmentNo">Enrollment Number</label>
        <input
          type="number"
          id="enroll"
          value={enrollmentNo}
          onChange={(event) => setenrollmentNo(event.target.value)}
          required
        />
      </div>
      <div className="form-group2">
        <label htmlFor="email">Email address</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
          required
        />
      </div>
      <div className="form-group2">
        <label htmlFor="password">Password</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          required
        />
      </div>
      <div className="form-group2">
        <label htmlFor="confirmPassword">Confirm Password</label>
        <input
          type="password"
          id="confirmPassword"
          value={confirmPassword}
          onChange={(event) => setConfirmPassword(event.target.value)}
          required
        />
      </div>
      <div>
        <label>
          <input
            type="checkbox"
            checked={isChecked}
            onChange={handleChange}
          /><a href='*'>I read and agree to the terms and conditions.</a>
        </label>
      </div>
      <button onClick={(e) => handleSignup(e)} type="submit" className='registerButton'>Register</button>
      <p>Already have an account?<a href='/login'>Login</a></p>
    </form>
  );
};

export default RegistrationForm;
