import { Link, Outlet } from 'react-router-dom';
import '../css/header.css';
import { useState } from 'react';
import { useEffect } from 'react';
import Cookies from 'js-cookie';

const Header = () => {
  const [cookies,setCookie]  = useState()
  const handleLogout=(e)=>{
    Cookies.remove("pass")
    window.location.reload(true)
  }
  useEffect(()=>{
    const cookie = Cookies.get("pass")
    if(cookie){
      setCookie(cookie)
    }
  },[cookies])
  return (
    <>
      <header className="header">
        <Link to="/" className="logo">Digital Pass</Link>
        <nav>
          <ul className="nav-links">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About Us</Link></li>
            <li><Link to="/contact">Contact</Link></li>
            {cookies ? 
            <>
              <li><Link to="/feedback">Feedback</Link></li>
              <li onClick={(e)=>handleLogout(e)}><Link to="/">logout</Link></li>
            </>:
            <>
              <li><Link to="/login" className="orange-border">Login</Link></li>
              <li><Link to="/signup" className="orange-border">Sign Up</Link></li>
            </>}
           
            
          </ul>
        </nav>
      </header>
      <Outlet />
    </>
  );
};

export default Header;
