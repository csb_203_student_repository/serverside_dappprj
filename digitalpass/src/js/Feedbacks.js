import React, { useEffect, useState } from "react";
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/dashboard.css';

const Feedback = () => {
    const [feedbackData, setFeedbackData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get("http://localhost:4005/api/v1/feedback");
                console.log(response.data.data)
                setFeedbackData(response.data.data);
            } catch (error) {
                console.log(error);
            }
        };

        fetchData();
    }, []);

    return (
        <div className="container-fluid px-0 bg-white " id="page-content-wrapper">
            <nav className="navbar navbar-expand-lg navbar-light bg-white py-4 px-4">
                <div className="d-flex align-items-center border-bottom border-dark pb-4 w-100 mb-3">
                    <h1 className="fs-2 m-0">Feedback</h1>
                </div>
            </nav>

            <div className="row my-2">
                <h3 className="fs-3 mb-3">Feedbacks</h3>
                <div className="col">
                    <table className="table border-dark table-bordered bg-white">
                        <thead className="border-bottom border-dark">
                            <tr>
                                <th scope="col">Enrollment No.</th>
                                <th scope="col">Name</th>
                                <th scope="col">frequency</th>
                                <th scope="col">features</th>
                            </tr>
                        </thead>
                        <tbody>
                            {feedbackData?.map((feedback) => (
                                <tr key={feedback.ownerData.enrollmentNumber}>

                                    <td>{feedback.ownerData.enrollmentNumber}</td>
                                    <td>{feedback.ownerData.name}</td>

                                    <td>{feedback.frequency}</td>
                                    <td>{feedback.features}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default Feedback;
