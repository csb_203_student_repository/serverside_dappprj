import React, { useState } from 'react';
import { Nav } from 'react-bootstrap';
import { Link, Outlet } from 'react-router-dom';
import '../css/sidebar.css';
import '../css/dashboard.css';
import Dashboard from '../img/dashboard.png';
import Certificate from '../img/certificate.png';
import Feedback from '../img/feedback.png'
import Logout from '../img/logout.png';
import Cookies from 'js-cookie';

function Sidebar() {
  const [navCollapse, setNavCollapse] = useState(false);
  const [smallNavCollapse, setSmallNavCollapse] = useState(false);
  function lohoutHandle(){
    // e.preventDefault()
    Cookies.remove("pass")
  }

  return (
    <div className={`d-flex ${smallNavCollapse ? 'smallNav' : ''}`}>
      <div className={`sidebar-container ${navCollapse ? 'navCollaps' : ''}expanded`}>
        <div className="bg-dark" id="sidebar-wrapper">
          <div className="sidebar-heading text-center py-4 primary-text fs-4 ">
            <div className="logo">
              <h4 className="text-white" style={{ textAlign: 'center',width:"250px" }}>DIGITAL PASS</h4>
              <i
                className="bi bi-justify largeDeviceIcon"
                onClick={(e) => setNavCollapse(!navCollapse)}
              ></i>
              <i
                className="bi bi-justify smallDeviceIcon"
                onClick={(e) => setSmallNavCollapse(!smallNavCollapse)}
              ></i>
            </div>
          </div>
          <div className="list-group list-group-flush my-3 pb-4 px-4 mb-3">
            <Nav className="flex-column d-flex flex-column flex-grow-1">
              <Nav.Item>
                <Nav.Link
                  as={Link}
                  to="/db"
                  className="list-group-item list-group-item-action justify-content-between bg-transparent second-text active mb-2"
                >
                  <img
                    height={"22%"}
                    width={"22%"}
                    src={Dashboard}
                    className="img-fluid mr-2"
                    alt=""
                  ></img>
                  Dashboard
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  as={Link}
                  to="/adminfeedback"
                  className="list-group-item list-group-item-action justify-content-between bg-transparent second-text active mb-4"
                >
                  <img
                    height={"25%"}
                    width={"25%"}
                    src={Feedback}
                    className="img-fluid mr-2"
                    alt=""
                  ></img>
                  Feedback
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  as={Link}
                  to="/certificate"
                  className="list-group-item list-group-item-action justify-content-between bg-transparent second-text active mb-4"
                >
                  <img
                    height={"25%"}
                    width={"25%"}
                    src={Certificate}
                    className="img-fluid"
                    alt=""
                  ></img>
                  Issue Certificate
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link onClick={()=>lohoutHandle()}
                  as={Link}
                  to="/"
                  className="list-group-item list-group-item-action justify-content-between bg-transparent second-text active mb-4"
                >
                  <img
                    height={"22%"}
                    width={"22%"}
                    src={Logout}
                    className="img-fluid mr-2"
                    alt=""
                  ></img>
                  Logout
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>
        </div>
      </div>
      <div className="details-section">
        <Outlet />
      </div>
    </div>
  );
}

export default Sidebar;
