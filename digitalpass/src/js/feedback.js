import React, { useState } from 'react';
import '../css/feedback.css';
import Cookies from 'js-cookie';
import axios from 'axios';
import { toast,ToastContainer } from 'react-toastify';

const FeedbackForm = () => {
  const [usage, setUsage] = useState('');
  const [features, setfeatures] = useState('');

  const handleSubmit = async(event) => {
    const toastOption = {
      position: "top-center",
      autoClose: 2000,
      pauseOnHover: true,
      draggable: true,
      theme: "dark",
    }
    event.preventDefault();
    try {
      const token = Cookies.get("pass")
      // alert(token)
      if(token){
        const respond = await axios({
          method:"POST",
          url:"http://localhost:4005/api/v1/feedback",
          data:{
            frequency:usage,
            features:features,
            token:token
          }
        }) 
        // alert(respond.data.status)
        if(respond.data.status){
          toast.success("Thank You For your Feedback",toastOption)
          setTimeout(() => {
            window.location.reload(true)
          }, 3000);
        }
      }
    } catch (err) {
      console.log(err)
    }

  }

  return (
    <form className="feedback-form" onSubmit={handleSubmit}>
      <ToastContainer/>
      <h2>Help Us To Improve!</h2>
      <div className="form-group3">
        <label htmlFor="text">How often do you use the website?</label>
        <input
          type="text"
          id="usage"
          value={usage}
          onChange={(event) => setUsage(event.target.value)}
          required
        />
      </div>
      <div className="form-group3">
        <label htmlFor="textarea">Some features that need to be improved.</label>
        <input
          type="features"
          id="features"
          value={features}
          onChange={(event) => setfeatures(event.target.value)}
          required
        />
      </div>
      <button type="submit" className='feedbackButton'>Submit</button>
    </form>
  );
};

export default FeedbackForm;
