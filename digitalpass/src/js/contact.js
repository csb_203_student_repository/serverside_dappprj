import React from 'react';
import "../css/contact.css";
import myimage3 from "../img/message.png";
import myimage2 from "../img/phone.png";
import myimage1 from "../img/location.png";
import Footer from './footer';
const Contact = () => {
  return (
    <>
    <div>
      <div class="contact-section1">
        <div class="background-image"></div>
        <div class="section-content">
          <h1 class="section-title">Contact Us</h1>
        </div>
      </div>
      <div class="contact-section2">
        <div class="section-content2">
          <h2 class="section-subtitle">Get In Touch</h2>
          <p class="section-description">Send us some details about your upcoming project and feel free to ask questions about our process. We’ll get back to you within 24 hours </p>
          <div class="cards-container">
            <div class="card">
              <img src={myimage1} alt="Service 1" />
              <h3>Our Location</h3>
              <p>Mongar / Gyalpozhing</p>
            </div>
            <div class="card">
              <img src={myimage2} alt="Service 2" />
              <h3>Call Us On</h3>
              <p>17282828</p>
            </div>
            <div class="card">
              <img src={myimage3} alt="Service 3" />
              <h3>Email Us</h3>
              <p>123.gcit@rub.edu..bt</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <Footer />
    </>
  );
};

export default Contact;