import React, { useState ,useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/dashboard.css';
import { Form, Button } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Web3 from 'web3';
import { CERTIFICATE_LIST_ABI, CERTIFICATE_LIST_ADDRESS } from "./../abi/config_certificate";

const Certificate = () => {
  const toastOption = {
    position: "top-center",
    autoClose: 2000,
    pauseOnHover: true,
    draggable: true,
    theme: "dark",
  };
  const [certificateContract, setCertificateContract] = useState(null);

  const loadBlockchainData = async () => {
    try {
      const web3 = new Web3(Web3.givenProvider || 'http://localhost:7545');
      const certificateContract = new web3.eth.Contract(
        CERTIFICATE_LIST_ABI,
        CERTIFICATE_LIST_ADDRESS
      );
      setCertificateContract(certificateContract);
    } catch (error) {
      console.error(error.message);
    }
  };
  useEffect(() => {
    loadBlockchainData();
  }, []);

  const [studentName, setStudentName] = useState('');
  const [field, setField] = useState('');
  const [signatureBy, setSignatureBy] = useState('');
  const [enrollmentNumber, setEnrollmentNumber] = useState('');

  const handleIssueButton = async (e) => {
    e.preventDefault();
    try {
      // alert("asd")
      if (!window.ethereum) {
        toast.error('MetaMask is not installed.', toastOption);
        return;
      }
      await window.ethereum.request({ method: 'eth_requestAccounts' });
      const web3 = new Web3(window.ethereum);
      const accounts = await web3.eth.getAccounts();
      if (accounts[0]) {
        const receipt = await certificateContract?.methods
          .issueCertificate(studentName, signatureBy, field, enrollmentNumber)
          .send({ from: accounts[0] });
        console.log(receipt);
        toast.success('Certificate issued successfully', toastOption);
      }


    } catch (error) {
      console.log(error);
      if (error.message.includes("revert already issued" || "already issued")) {
        toast.error("Already Issued", toastOption);
    }
  };
}


  return (
    <div className="container-fluid px-0 bg-white" id="page-content-wrapper">
      <ToastContainer />
      <nav className="navbar navbar-expand-lg navbar-light bg-white py-4 px-4">
        <div className="d-flex border-bottom border-dark pb-4 px-0 w-100 mb-3">
          <h1 className="fs-2 m-0">Issue Certificate</h1>
        </div>
      </nav>

      <Form className="my-form border border-dark p-5">
        <Form.Group className="mb-2" controlId="formName">
          <Form.Label>Full Name</Form.Label>
          <Form.Control
            type="text"
            value={studentName}
            onChange={(e) => setStudentName(e.target.value)}
            placeholder="Enter name"
          />
        </Form.Group>

        <Form.Group className="mb-2" controlId="formId">
          <Form.Label>Enrollment No</Form.Label>
          <Form.Control
            value={enrollmentNumber}
            onChange={(e) => setEnrollmentNumber(e.target.value)}
            type="text"
            placeholder="Enter ID"
          />
        </Form.Group>

        <Form.Group className="mb-2" controlId="courseId">
          <Form.Label>Course</Form.Label>
          <Form.Control
            value={field}
            onChange={(e) => setField(e.target.value)}
            type="text"
            placeholder="Enter the course"
          />
        </Form.Group>

        <Form.Group className="mb-4" controlId="formSignature">
          <Form.Label>Signature</Form.Label>
          <Form.Control
            type="text"
            value={signatureBy}
            onChange={(e) => setSignatureBy(e.target.value)}
            placeholder="Enter signature"
          />
        </Form.Group>

        <Form.Group className="mb-0" controlId="formSubmit">
          <Button onClick={handleIssueButton} type="submit" className="btn btn-dark px-4">
            Issue
          </Button>
        </Form.Group>
      </Form>
    </div>
  );
};

export default Certificate;
