import React from 'react';
import '../css/landingpage.css';
import myimage1 from "../img/secondImage.png";
import myimage2 from "../img/issue.png";
import myimage3 from "../img/laptop.png";
import myimage4 from "../img/auth.png";
import myimage5 from "../img/write.png";
import Footer from './footer';
import { Link } from 'react-router-dom';
const Home = () => {
    return (
        <>
        <div>
            <div className='section1'>
                <div className="image-container">
                    <div className="cta-container">
                        <h1 className="cta-title">Join Our Community Today</h1>
                        <p className="cta-description">Unlock the power of digital credentials with our Digital Certificate service. Say goodbye to paper certificates and embrace the convenience and security of digital records.</p>
                        <Link to="/signup"> <button className="signup-button">Sign Up</button></Link>
                    </div>
                </div>
            </div>
            <div className='section2'>
                <div className="container1">
                    <div className="image-container2">
                        <img src={myimage1} alt="campus" />
                    </div>
                    <div className="text-container2">
                        <h2 >Join the future of certification now!</h2>
                        <p>Are you tired of traditional paper certificates that can be easily lost, damaged or even faked? Upgrade to a digital certificate based on Blockchain technology today! With our secure and immutable digital certificates, you can easily verify your achievements anytime, anywhere</p>
                    </div>
                </div>
            </div>
            <div className="section3">
                <div className="background-image"></div>
                <div className="background-image"></div>
                <h1 className='section3_h1'>Services</h1>
                <div className='section3_inside'>
                    <div className="cards-container">
                        <div className="card">
                            <img src={myimage2} alt="issue" />
                            <h3>Efficient Issuance</h3>
                            <p>Issuing digital certificates is faster and more efficient than traditional paper certificates, reducing administrative costs and time</p>
                        </div>
                        <div className="card">
                            <img src={myimage4} alt="auth" />
                            <h3>Authenticity Verification</h3>
                            <p>Blockchain-based digital certificates are secure and tamper-proof, making it easy to verify their authenticity and prevent fraud</p>

                        </div>
                        <div className="card">
                            <img src={myimage3} alt="record" />
                            <h3>Immutable Record-Keeping</h3>
                            <p>Once a certificate is issued, it becomes a permanent record on the blockchain, making it impossible to modify or delete the certificate</p>

                        </div>
                    </div>
                </div>

            </div>
            <div class="section4">
                <div class="container2">
                    <div class="text-container3">
                        <h2>Join the digital revolution today!</h2>
                        <p>Say goodbye to the hassle of managing paper certificates and embrace the ease and security of digital certificates. Get started with our Blockchain-based digital certificates today and never worry about losing or damaging your achievements again</p>
                        <button class="read-more-button">Read More</button>
                    </div>
                    <div class="image-container3">
                        <img src={myimage5} alt="Section4"/>
                    </div>
                </div>
            </div>

        </div>
        <Footer />
        </>
    );
};

export default Home;