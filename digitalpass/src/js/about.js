import React from 'react';
import "../css/about.css"
import myimage1 from "../img/aboutussecondImage.png";
import myimage2 from "../img/aboutusthirdImage.png";
import myimage3 from "../img/naka.png";
import myimage4 from "../img/dhendup.png";
import myimage5 from "../img/pemayangzom.png";
import Footer from './footer';
const About = () => {
    return (
        <>
        <div>
            <div class="about-us-section1">
                <div class="background-image"></div>
                <div class="section-content">
                    <h1 class="section-title">About Us</h1>
                </div>
            </div>
            <div class="section2-about-us">
                <div class="container-about-us">
                    <div class="text-container-about-us">
                        <h2>Join us today and experience the future of certification!</h2>
                        <p>At Digital Pass, we are committed to providing our clients with the most secure and reliable digital certificate solutions based on Blockchain technology. Our team of experts is dedicated to delivering innovative and efficient certificate services that meet the needs of individuals, businesses, and organizations. With our state-of-the-art digital certificates, you can enjoy the benefits of easy and instant verification, secure and tamper-proof records, and convenient accessibility</p>
                    </div>
                    <div class="image-container-about-us">
                        <img src={myimage1} alt="image2" />
                    </div>
                </div>
            </div>
            <div class="section3-about-us">
                <div class="container-about-us2">
                    <div class="text-container-about-us2">
                        <h2>Benefits of Developing a Blockchain Based Digital Certificate</h2>
                        <p>Blockchain-based digital certificates offer a plethora of benefits over traditional paper-based certificates. One of the most significant benefits is increased security. Blockchain technology provides an immutable and tamper-proof record of each certificate, making it virtually impossible for fraudsters to modify or forge them. The decentralized nature of the blockchain network ensures that no central authority controls the certificates, further enhancing their security</p>
                        <p>Another advantage of blockchain-based digital certificates is increased efficiency. The issuance and verification of digital certificates are much faster and more streamlined than traditional paper-based certificates. With blockchain technology, certificates can be issued and verified instantly, eliminating the need for time-consuming manual processes. This enhanced efficiency can save businesses and organizations significant time and money and can also improve customer satisfaction by providing a convenient and efficient certification process</p>
                    </div>
                    <div class="image-container-about-us2">
                        <img src={myimage2} alt="image3" />
                    </div>
                </div>
            </div>
            <div className="section4">
                <div className="background-image"></div>
                <div className="background-image"></div>
                <h1 className='section4_h1'>Services</h1>
                <div className='section4_inside'>
                    <div className="cards-container">
                        <div className="card4">
                            <div className="card-image4">
                                <img src={myimage3} alt="issue" />
                            </div>
                            <h3>Tshering </h3>
                        </div>
                        <div className="card4">
                            <div className="card-image4">
                                <img src={myimage4} alt="issue" />
                            </div>
                            <h3>Dhendup Tshering </h3>
                        </div>
                        <div className="card4">
                            <div className="card-image4">
                                <img src={myimage5} alt="issue" />
                            </div>
                            <h3>Pema Yangzom </h3>
                        </div>
                        <div className="card4">
                            <div className="card-image4">
                                <img src={myimage3} alt="issue" />
                            </div>
                            <h3>Tashi Wangdi </h3>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <Footer />
        </>
    );
};

export default About;