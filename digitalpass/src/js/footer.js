import "../css/footer.css"
import { IoLogoFacebook, IoLogoInstagram, IoLogoTwitter } from 'react-icons/io5';
export default function Footer() {
    return (
        <>
            <div className="myfooter bg-dark">
                <div className="column about">
                    <h2>Digital Pass</h2>
                    <p>Blockchain-based digital certificates are secure and tamper-proof, making it easy to verify their authenticity and prevent fraud</p>
                </div>
                <div className="column line"></div>
                <div className="column contact">
                    <h3>Contact Us</h3>
                    <div>
                        <h5>Email</h5>
                        <p>123.gcit@rub.edu.bt</p>
                    </div>
                    <div>
                        <h5>Phone Number</h5>
                        <p>17282828</p>
                    </div>
                </div>
                <div className="column line"></div>
                <div className="column follow">
                    <h3>Follow Us</h3>
                    <div className="logos">
                        <IoLogoFacebook size={32} color="white" />
                        <IoLogoTwitter size={32} color="white" />
                        <IoLogoInstagram size={32} color="white" />
                    </div>
                    <div className="copyright">
                        Copyright 2023<br></br>All Rights Reserved by Digital Pass
                    </div>
                </div>
            </div>
        </>
    )
}