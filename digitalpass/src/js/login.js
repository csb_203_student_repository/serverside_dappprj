import React, { useState } from 'react';
import '../css/login.css';
import axios from 'axios';
import Cookies from 'js-cookie';
import { ToastContainer, toast } from 'react-toastify';
import { useNavigate } from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';



const SignInAForm = () => {
  const navigate = useNavigate()
  const toastOption = {
    position: "top-center",
    autoClose: 2000,
    pauseOnHover: true,
    draggable: true,
    theme: "dark",
  }

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  const handleSubmit = async (event) => {
    event.preventDefault();
    const respond = await axios({
      method: "POST",
      url: "http://localhost:4005/api/v1/users/login",
      data: {
        email,
        password
      }
    })
    // console.log(respond.data.data.user)
    console.log(respond.data)
    if (respond.data.status === "success") {
      Cookies.set('pass', respond.data.token, { path: '/' });
      if (respond.data.data.user.role === "student") {
        toast.success("Login Successfull", toastOption)
        setTimeout(() => {
          navigate("/index")
        }, 3000);
      }else{
        toast.success("Login Successfull", toastOption)
        setTimeout(() => {
          navigate("/db")
        }, 3000);
      }

    }
  }

  return (
    <>
      <ToastContainer />
      <div className='firstSection'>
        <form className="registration-form1" onSubmit={handleSubmit}>
          <h2>Sign In</h2>
          <div className="form-group1">
            <label htmlFor="email">Email address</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              required
            />
          </div>
          <div className="form-group1">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              required
            />
          </div>
          <p>Forgot Password?<a href='*'>Click here</a></p>
          <button type="submit" className='loginButton'>Login</button>
        </form>
      </div>
    </>

  );
};

export default SignInAForm;
