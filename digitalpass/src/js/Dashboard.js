import React, { useEffect, useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import '../css/dashboard.css';
import axios from "axios";

const Dashboard = ({ certificateContract }) => {
    const [cert, setCert] = useState()
    const [users, setUser] = useState([])
    // const
    const fetchCertificate = async () => {
        try {
            const receipt = await certificateContract?.methods.certificateNumber().call();
            // console.log("asdas", receipt);
            // alert(receipt.date)
            setCert(Number(receipt))
        } catch (error) {
            console.log(error);
        }
    };

    const getAlluser = async () => {
        try {
            const respond = await axios.get("http://localhost:4005/api/v1/users")
            if (respond.data.status === "success") {
                const members = [];
                respond.data.data.map((value) => {
                    members.push(value)
                })
                // console.log("asd",members)
                setUser(members);

            }
        } catch (err) {
            console.log(err)
        }

    }
    useEffect(() => {
        getAlluser()
        fetchCertificate()
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <div className="container-fluid px-0 bg-white " id="page-content-wrapper">
            <nav className="navbar navbar-expand-lg navbar-light bg-white py-4 px-4">
                <div className="d-flex align-items-center border-bottom border-dark pb-4 w-100 mb-3">
                    <h1 className="fs-2 m-0">Dashboard</h1>
                </div>
            </nav>
            <div className="container-fluid px-0">
                <div className="row g-4 ">
                    <div className="col-md-6">
                        <div className="p-3 bg-dark text-light shadow-sm d-flex justify-content-around align-items-center rounded">
                            <div>
                                <p className="fs-5">Total Users</p>
                                <h3 className="fs-2">{users.length}</h3>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="p-3 bg-dark text-light shadow-sm d-flex justify-content-around align-items-center rounded">
                            <div>
                                <p className="fs-5">Certificate Issued</p>
                                <h3 className="fs-2">{cert}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row my-5">
                <h3 className="fs-4 mb-3">Users</h3>
                <div className="col">
                    <table className="table border-dark table-bordered bg-white">
                        <thead className="border-bottom border-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Email Address</th>
                                <th scope="col">Enrollment No.</th>
                                <th scope="col">Access</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                users.map((value) => {
                                    return (
                                        <tr>
                                            <td>{value.name}</td>
                                            <td>{value.email}</td>
                                            <td>{value.enrollmentNumber}</td>
                                            <td>Accepted</td>
                                            <td>Active</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default Dashboard;
