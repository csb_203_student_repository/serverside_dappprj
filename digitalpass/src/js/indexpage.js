import React, { useEffect, useState } from 'react';
import '../css/indexpage.css';
import '../css/certificate.css';
import Footer from './footer';
import image1 from "../img/gcitlogo.webp";
import Cookies from 'js-cookie';
import axios from 'axios';
const Index = ({ certificateContract }) => {
  const [cert, setCert] = useState()
  const [enrollmentNumber, setEnrollmentNumber] = useState()
  const loadUserDataWithCookie = async () => {
    try {
      const token = Cookies.get('pass');
      if (token) {
        await axios({
          method: 'POST',
          url: 'http://localhost:4005/api/v1/users/getcookiedetails',
          data: {
            token
          }
        }).then((response) => {
          setEnrollmentNumber(response.data.data.freshUser.enrollmentNumber)
          fetchCertificate(response.data.data.freshUser.enrollmentNumber)
        })
      }
    } catch (err) {
      console.log(err)
    }
  }
  const fetchCertificate = async () => {
    try {
      const receipt = await certificateContract.methods.getCertificate(enrollmentNumber).call();
      console.log("asdas", receipt);
      // alert(receipt.date)
      const date = new Date(receipt.date * 1000);
      const dateString = date.toLocaleString();
      setCert({
        date: dateString,
        field: receipt.field,
        signatureBy: receipt.signatureBy,
        studentName: receipt.studentName
      })
      // handleDate(receipt.date)

    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    loadUserDataWithCookie()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enrollmentNumber]);

  return (
    <>
      <div>
        <div className='section1'>
          <div className="image-container">
            <div className="section-content">
              <h1 className="section-title">Welcome To Digital Pass</h1>
            </div>
          </div>
        </div>
        <div className='section2'>
          <div className='title' style={{ marginBottom: "30px" }}>
            <h1>View Your Details</h1>
          </div>
          <div className="container pm-certificate-container">
            <div className="outer-border"></div>
            <div className="inner-border"></div>

            <div className="pm-certificate-border col-xs-12">
              <div className="row pm-certificate-header">
                <div className="pm-certificate-title cursive col-xs-12 text-center">
                  <h2>Certificate of Recognition</h2>
                </div>
              </div>

              <div className="row pm-certificate-body">
                <div className="pm-certificate-block">
                  <div className="col-xs-12">
                    <div className="row">
                      <div className="col-xs-2"></div>
                      <div className="pm-certificate-name underline margin-0 col-xs-8 text-center">
                        <span className="pm-name-text bold">Presented to</span>
                      </div>
                      <div className="col-xs-2"></div>
                    </div>
                  </div>

                  <div className="col-xs-12">
                    <div className="row">
                      <div className="col-xs-2"></div>
                      <div className="pm-earned col-xs-8 text-center">
                        <span className="pm-credits-text block bold sans">{cert?.studentName}</span>
                      </div>
                      <div className="col-xs-2"></div>
                      <div className="col-xs-12"></div>
                    </div>
                  </div>

                  <div className="col-xs-12">
                    <div className="row">
                      <div className="col-xs-2"></div>
                      <div className="pm-course-title col-xs-8 text-center"></div>
                      <div className="col-xs-2"></div>
                    </div>
                  </div>

                  <div className="col-xs-12">
                    <div className="row">
                      <div className="col-xs-2"></div>
                      <div className="pm-course-title underline col-xs-8 text-center">
                        <span className="pm-credits-text block bold sans">For: {cert?.field}</span>
                      </div>
                      <div className="col-xs-2"></div>
                    </div>
                  </div>
                </div>

                <div className="col-xs-12 text-center">
                  <div className="circle-shape"></div>
                  <div className="logo-container">
                    <img
                      src={image1}
                      alt="College Logo"
                      className="college-logo"
                    />
                  </div>
                </div>

                <div className="col-xs-12">
                  <div className="row">
                    <div className="pm-certificate-footer">
                      <div className="col-xs-4 pm-certified col-xs-4 text-center">
                        <span className="pm-credits-text block sans">Gyalpozhing College of Information and Technology</span>
                        <span className="pm-empty-space block underline"></span>
                        <span className="bold block"></span>
                      </div>
                      <div className="col-xs-4"></div>
                      <div className="col-xs-4 pm-certified col-xs-4 text-center">
                        <span className="pm-credits-text block sans">{cert?.date}</span>
                        <span className="pm-empty-space block underline"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Index;
