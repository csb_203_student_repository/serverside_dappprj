const express = require("express")
const userRoute = require("./Routes/userRoute")
const feedbackRoute = require("./Routes/feedbackRoute")
const cors = require('cors');


const app = express()
app.use(cors());
app.use(express.json())
app.use('/api/v1/users',userRoute)
app.use("/api/v1/feedback",feedbackRoute)


module.exports =app;
