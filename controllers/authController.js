const User = require('./../models/userModels')
const jwt = require('jsonwebtoken')
const promisify = require('util').promisify


const signToken = (id)=>{
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn:process.env.JWT_EXPIRES_IN
    })
}
const createSendToken = (user,statusCode,res)=>{
    const token = signToken(user._id)
    res.status(statusCode).json({
        status:'success',
        token,
        data:{
            user
        }
    })

}
exports.signup = async(req,res,next)=>{
    try{
        const newUser = await User.create(req.body)
        createSendToken(newUser,201,res)
    }catch(err){
        res.status(500).json({error:err.message})
    }
}
exports.login = async(req,res,next)=>{
    try{
        // console.log(req.headers)
        const {email,password} = req.body
        if(!email || !password){
            console.log("either phonenumber or password is empty")
        }
        const user = await User.findOne({email}).select('+password')
        console.log(user)
        // console.log(user[0].password)
        // const correct = await User.correctPassword(password,user.password)
        if (!user || !(await user.correctPassword(password, user.password))){
            // console.log("incorrect password")
            res.status(500).json({error:"check your email or password"})
        }else{
            createSendToken(user,200,res)
        }
        next()

    }catch(err){
        console.log(err)
        res.status(500).json({error:err.message})
    }
}


exports.getDetailOfCookie = async (req, res, next) => {
    try {
        const decoded = await promisify(jwt.verify)(req.body.token, process.env.JWT_SECRET)
        console.log(decoded)
        // 3) check if user still exits
        const freshUser = await User.findById(decoded.id)
        if (!freshUser) {
            console.log('the user belonging to this token no longer exist')
        }
        console.log(freshUser)
        //Grant access to protected route
        res.status(200).json({
            status: 'success',
            data: {
                freshUser
            }
        })
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}











exports.protect = async(req,res,next)=>{
    try{
        //1) getting token and check of its there
        // const token  = req.body.token;


        // 2) Verificatin token
        console.log(req.body.token)
        const decoded = await promisify(jwt.verify)(req.body.token, process.env.JWT_SECRET)
        console.log(decoded)


        // 3) check if user still exits
        const freshUser = await User.findById(decoded.id)
        if(!freshUser){
            console.log('the user belonging to this token no longer exist')
            
        }
        console.log(freshUser)
        //Grant access to protected route
        req.user =freshUser
        next()


    }catch(err){
        res.status(500).json({error:err.message})
    }
}



// // changing password
// exports.updatePassword = async(req,res,next)=>{
//     try{
//         // 1) get the user from collection
//         const user = await User.findById(req.user.id).select('+password')//?????????(req.user.id)

//         // 2) check if posted current password is correct
//         if(!(await user.correctPassword(req.body.passwordCurrent, user.password))){
//             return next(new AppError('your current password is wrong'),401)
//         }

//         // 3) if so, update password
//         user.password = req.body.password
//         user.passwordConfirm = req.body.passwordConfirm
//         await user.save()

//         // 4) log user in, send jwt 
//         createSendToken(user,200,res)
//     }catch(err){
//         res.status(500).json({error:err.message})
//     }
// }




















// exports.login =async (req,res,next) => {
//     try{
//         const {email,password} =req.body
//         //1)check if email and password exist
//         if(!email || !password){
//             return next(new AppError("Please provide an email and password!", 400))
//         }
//         //2) check if user exists && password is correct
//         const user =await User.findOne({email}).select("+password")
        

//         if (!user || !(await user.correctPassword(password, user.password))){
//             console.log("aksdlksjd;lj;l")
//         }
//         //3) if everything ok, send token to client
//         // createSendToken(user,200,res)
//         // res.status(200).json({
//         //     status: "success",
//         //     token,
//         // })

//     }catch (err){
//         res.status(500).json({error:err.message});

//     }
// }
