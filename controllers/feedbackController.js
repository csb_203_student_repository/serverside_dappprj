const FeedBackModel = require("./../models/feedbackModal")
exports.createFeedback = async(req,res,next)=>{
    const feedback = new FeedBackModel({...req.body,ownerData:req.user._id})
    try{
        const feedbackData = await feedback.save()
        res.json({data:feedbackData,status:"success"})

    }catch(err){
        res.statue(500).json({error:err.message})
    }
}

exports.getAllFeedbackDetails = async(req,res,next)=>{
    try{
        const feedbackData = await FeedBackModel.find().populate('ownerData')
        // const bookingDataAndUserData = await bookingData.populate('ownerData')
        res.status(200).json({data:feedbackData,status:"success"})

    }catch(err){
        res.status(500).json({error:err.message})
    }
}
exports.updateFeedbackDataUsingId = async(req,res,next)=>{
    try {
        const feedback =await FeedBackModel.findByIdAndUpdate(req.params.id,req.body);
        res.json({ data: feedback,status:"success"});
    }catch(err){
        res.status(500).json({error: err.message});
    }

}