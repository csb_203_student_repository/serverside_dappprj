const mongoose = require('mongoose')
const validator = require('validator')

const bcrypt = require("bcrypt")

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true,'Please tell us your name!']
    },
    enrollmentNumber:{
        type:Number,
        unique:true,
        required:[true,"Please provide a phone Number"],

    },
    email:{
        type:String,
        unique:true,
        lowercase:true,
        validate:[validator.isEmail,"Please provide a valid email"]
    },
    role:{
        type:String,
        enum:['student','admin'],
        default:'student'
    },
    password:{
        type:String,
        required:[true,"Please provide a password!"],
        minlength:8,
        select:false
    },
    passwordConfirm:{
        type:String,
        require:[true,"Please confirm password"],
        validate:{
            validator:function(el){
                return el === this.password
            },
            message:"Password are not same",
        }

    },
    active:{
        type:Boolean,
        default:true,
        select:false
    },

})
userSchema.pre('save',async function(next){
    if (!this.isModified('password')) return next()
    this.password = await bcrypt.hash(this.password,12)
    this.passwordConfirm = undefined
    next()
})
userSchema.methods.correctPassword =async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User',userSchema)
module.exports= User