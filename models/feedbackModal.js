const { default: mongoose } = require("mongoose")
// const User = require("./User")
const feedbackSchema =new mongoose.Schema({
    frequency:{
        type:String,
        require:true,
        default:"No feedback"
    },
    features:{
        type:String,
        default:true
    },
    ownerData:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:"User"
    }
})

const FeedBackModel =mongoose.model('FeedBackModel',feedbackSchema)
module.exports = FeedBackModel