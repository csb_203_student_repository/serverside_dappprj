const express = require('express')
const feedbackController = require('./../controllers/feedbackController')
const authController = require('./../controllers/authController')

const router = express.Router()
router.post('/',authController.protect,feedbackController.createFeedback)
router.get('/',feedbackController.getAllFeedbackDetails)
// router.post('/getcookiedetails', feedbackController.)
// router
//     .route('/')
//     .get(userController.getAllUser)



module.exports = router