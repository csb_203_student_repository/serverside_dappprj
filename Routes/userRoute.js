const express = require('express')
const userController = require('./../controllers/userController')
const authController = require('./../controllers/authController')
const router = express.Router()
router.post('/signup',authController.signup)
router.post('/login',authController.login)
router.post('/getcookiedetails', authController.getDetailOfCookie)
router
    .route('/')
    .get(userController.getAllUser)



module.exports = router