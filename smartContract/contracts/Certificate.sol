// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

contract IssueCertificate {
    uint public certificateNumber;
    address public administrator;

    constructor() {
        administrator = msg.sender;
    }

    struct Certificate {
        uint certificateID;
        string studentName;
        string field;
        string signatureBy;
        uint date;
    }

    Certificate[] public certificates;
    mapping(uint => uint) public mapEnrollWithCertificateID;

    event issueCertificateEvent(string studentName);

    function issueCertificate(
        string memory _studentName,
        string memory _signatureBy,
        string memory _field,
        uint enrollmentNumber
    ) public {
   
        require(mapEnrollWithCertificateID[enrollmentNumber] == 0, "already issued");
        certificateNumber++;
        Certificate memory newCertificate = Certificate({
            certificateID: certificateNumber,
            studentName: _studentName,
            field: _field,
            signatureBy: _signatureBy,
            date: block.timestamp
        });
        certificates.push(newCertificate);
        mapEnrollWithCertificateID[enrollmentNumber] = certificateNumber;
        // emit issueCertificateEvent(_studentName);
    }

    function getCertificate(uint _enrollment) public view returns (Certificate memory) {
        uint index = mapEnrollWithCertificateID[_enrollment] - 1;
        Certificate memory certificate = certificates[index];
        certificate.certificateID = uint(certificate.certificateID); // Convert BigNumber to uint
        return certificate;
    }
}
